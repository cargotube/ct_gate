%%
%% Copyright (c) 2018-2020 Bas Wegh
%%

-module(ct_gate_in).

-include_lib("ct_msg/include/ct_msg.hrl").

-behaviour(gen_statem).

%%
-export([start_link/1]).
-export([set_serializer/2]).
-export([handle_raw_data/2]).
-export([close_session/1]).
-export([stop/1]).
%% gen_statem.
-export([init/1]).
-export([callback_mode/0]).
-export([handle_event/4]).
-export([terminate/3]).
-export([code_change/4]).

-record(data,
        {peer_pid = undefined,
         serializer = undefined,
         max_length = 0,
         buffer = <<"">>,
         message_queue = [],
         ping_time = undefined,
         ping_payload = undefined,
         session = undefined}).

start_link(Config) when is_map(Config) ->
    Data = create_initial_data(Config),
    gen_statem:start_link(?MODULE, Data, []).

create_initial_data(#{transport_type := TransportType,
                      peer_cert := Cert,
                      peer_ip := IP,
                      peer_port := Port} =
                        Config) ->
    #data{peer_pid = self(),
          serializer = maps:get(serializer, Config, undefined),
          session = cta_session:new(IP, Port, Cert, TransportType)}.

set_serializer(Serializer, Pid) ->
    gen_statem:call(Pid, {serializer, Serializer}).

handle_raw_data(Data, Pid) ->
    gen_statem:cast(Pid, {raw_data, Data}).

close_session(Pid) ->
    gen_statem:call(Pid, session_close).

stop(Pid) ->
    gen_statem:stop(Pid).

%% use the handle event function
callback_mode() ->
    handle_event_function.

init(Data) ->
    {ok, expect_hello, Data}.

handle_event(cast, {raw_data, InData}, State, Data) ->
    lager:debug("[~p] >>> ~p", [self(), InData]),
    handle_raw_data(InData, State, Data);
handle_event({call, {Peer, _} = From},
             session_close,
             State,
             #data{peer_pid = Peer} = Data) ->
    {next_state, State, reset_data_close_session(Data), [{reply, From, ok}]};
handle_event({call, {Peer, _} = From},
             {serializer, Serializer},
             State,
             #data{peer_pid = Peer} = Data) ->
    {next_state, State, Data#data{serializer = Serializer}, [{reply, From, ok}]};
handle_event(info, next_message, State, #data{message_queue = [Message | Tail]} = Data) ->
    trigger_next_message(),
    Type = ct_msg:get_type(Message),
    NewData = Data#data{message_queue = Tail},
    lager:debug("[~p] --> ~p", [self(), Message]),
    {_Time, Result} =
        timer:tc(fun handle_incoming_wamp_message/4, [State, Type, Message, NewData]),
    Result;
handle_event(info, next_message, State, #data{message_queue = []} = Data) ->
    activate_connection_once(Data),
    {next_state, State, Data};
handle_event(info, {to_peer, Message}, State, Data) ->
    Type = ct_msg:get_type(Message),
    handle_outgoing_wamp_message(State, Type, Message, Data);
handle_event(Event, Content, State, Data) ->
    lager:debug("[~p] ignore event [~p] ~p ~p", [self(), State, Event, Content]),
    {next_state, State, Data}.

handle_raw_data(RawData, State, Data) ->
    {Messages, NewData} = deserialize_messages_update_data(RawData, Data),
    set_wamp_message_queue(Messages, State, NewData).

set_wamp_message_queue([], State, Data) ->
    activate_connection_once(Data),
    {next_state, State, Data};
set_wamp_message_queue(Messages, State, Data) ->
    trigger_next_message(),
    {next_state, State, Data#data{message_queue = Messages}}.

handle_incoming_wamp_message(expect_hello,
                             hello,
                             Hello,
                             #data{session = Session} = Data) ->
    ct_routing:handle_hello(Hello, Session),
    {next_state, welcome_or_challenge, Data};
handle_incoming_wamp_message(expect_hello, _Type, _Message, Data) ->
    serialize_and_send_to_peer(?ABORT(#{}, canceled), Data),
    {next_state, expect_hello, reset_data_close_session(Data)};
handle_incoming_wamp_message(expect_authenticate,
                             authenticate,
                             Auth,
                             #data{session = Session} = Data) ->
    ct_routing:handle_authenticate(Auth, Session),
    {next_state, welcome, Data};
handle_incoming_wamp_message(established, goodbye, _Message, Data) ->
    serialize_and_send_to_peer(?GOODBYE(#{}, goodbye_and_out), Data),
    {next_state, expect_hello, reset_data_close_session(Data)};
handle_incoming_wamp_message(expect_goodbye, goodbye, _Message, Data) ->
    {next_state, expect_hello, reset_data_close_session(Data)};
handle_incoming_wamp_message(established, Type, Message, #data{session = Session} = Data)
    when Type == error;
         Type == publish;
         Type == subscribe;
         Type == unsubscribe;
         Type == call;
         Type == register;
         Type == unregister;
         Type == yield ->
    %% TODO: check what type the peer is and allow only messages for that
    %% type e.g. caller
    ct_routing:handle_established_message(Message, Session),
    {next_state, established, Data};
handle_incoming_wamp_message(State, ping, {ping, Payload}, Data) ->
    send_to_peer(ct_msg:pong(Payload), Data),
    {next_state, State, Data};
handle_incoming_wamp_message(State,
                             pong,
                             {pong, Payload},
                             #data{ping_payload = Payload} = Data) ->
    {next_state, State, Data};
handle_incoming_wamp_message(State, pong, _, Data) ->
    %% bad/old pong reply - just ignore it
    {next_state, State, Data};
handle_incoming_wamp_message(expect_goodbye, Message, _, Data) ->
    lager:debug("[~p] bad message ~p; kill connection", [self(), Message]),
    close_connection(Data);
handle_incoming_wamp_message(_, Message, _, Data) ->
    lager:debug("[~p] bad message ~p; goodbye and kill connection", [Message, self()]),
    serialize_and_send_to_peer(?GOODBYE(#{}, canceled), Data),
    close_connection(Data).

handle_outgoing_wamp_message(State, welcome, Welcome, Data)
    when State == welcome_or_challenge; State == welcome ->
    UpdatedData = set_session_id(Welcome, Data),
    serialize_and_send_to_peer(Welcome, UpdatedData),
    {next_state, established, UpdatedData};
handle_outgoing_wamp_message(State, abort, Abort, Data)
    when State == welcome_or_challenge; State == welcome ->
    serialize_and_send_to_peer(Abort, Data),
    {next_state, expect_hello, reset_data_close_session(Data)};
handle_outgoing_wamp_message(welcome_or_challenge, challenge, Clg, Data) ->
    serialize_and_send_to_peer(Clg, Data),
    {next_state, expect_authenticate, Data};
handle_outgoing_wamp_message(established, Type, Message, Data)
    when Type == error;
         Type == published;
         Type == subscribed;
         Type == unsubscribed;
         Type == event;
         Type == result;
         Type == registered;
         Type == unregistered;
         Type == invocation;
         Type == interrupt ->
    serialize_and_send_to_peer(Message, Data),
    {next_state, established, Data};
handle_outgoing_wamp_message(established, goodbye, Message, Data) ->
    serialize_and_send_to_peer(Message, Data),
    {next_state, expect_goodbye, Data};
handle_outgoing_wamp_message(State, _Type, Message, Data) ->
    lager:warning("[~p] throw away bad out message [~p] ~p", [self(), State, Message]),
    {next_state, established, Data}.

set_session_id(Welcome, #data{session = Session} = Data) ->
    {ok, SessionId} = ct_msg:extract_session(Welcome),
    lager:debug("[~p] SessionId: ~p", [self(), SessionId]),
    Data#data{session = cta_session:set_id(SessionId, Session)}.

trigger_next_message() ->
    self() ! next_message.

deserialize_messages_update_data(TcpData,
                                 #data{buffer = OldBuffer, serializer = Ser} = Data) ->
    Buffer = <<OldBuffer/binary, TcpData/binary>>,
    {Messages, NewBuffer} = ct_msg:deserialize(Buffer, Ser),
    {Messages, Data#data{buffer = NewBuffer}}.

reset_data_close_session(#data{session = Session} = Data) ->
    ok = ct_routing:session_closed(Session),
    Data#data{max_length = 0, buffer = <<"">>, session = undefined}.

close_connection(#data{session = Session} = Data) ->
    ok = ct_routing:session_closed(Session),
    {stop, normal, Data}.

activate_connection_once(#data{peer_pid = Peer}) ->
    Peer ! connection_once.

serialize_and_send_to_peer(Msg, #data{serializer = Serializer} = Data) ->
    lager:debug("[~p] <-- ~p", [self(), Msg]),
    OutMsg = ct_msg:serialize(Msg, Serializer),
    send_to_peer(OutMsg, Data).

send_to_peer(Msg, #data{peer_pid = Peer}) ->
    lager:debug("[~p] <<< ~p", [self(), Msg]),
    Peer ! {connection_send, Msg}.

terminate(_Reason, _State, #data{session = Session} = Data) ->
    lager:debug("[~p] terminate", [self()]),
    ok = ct_routing:session_closed(Session),
    ok.

code_change(_OldVsn, State, Data, _Extra) ->
    {ok, State, Data}.
