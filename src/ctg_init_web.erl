-module(ctg_init_web).

%% for file access
-include_lib("kernel/include/file.hrl").

-export([start/2]).

start(Id, Config0) when is_map(Config0) ->
    Config = maps:put(transport_id, Id, Config0),
    UseSSL = web_use_ssl(Config),
    Options = web_options(UseSSL, Config),
    Dispatch = web_dispatch(Config),
    ok = start_tls_or_clear(UseSSL, Id, Options, Dispatch),
    ok.

web_use_ssl(Config) ->
    Cert = cert_file(Config),
    Key = key_file(Config),
    ctg_con_utils:use_ssl(Cert, Key).

web_dispatch(Config) ->
    cowboy_router:compile(web_routes(Config)).

web_routes(Config) ->
    WsPath = maps:get(ws_path, Config, "/"),
    StaticDir = maps:get(static_dir, Config, undefined),
    StaticPath = maps:get(static_path, Config, undefined),

    PathList =
        web_path_list([{ws_path, WsPath}, {static, StaticPath, StaticDir}], [], Config),
    lager:debug("web pathlist: ~p", [PathList]),
    [{'_', PathList}].

web_path_list([], List, _Config) ->
    List;
web_path_list([{ws_path, Path} | Tail], List, Config) ->
    web_path_list(Tail, [{Path, ct_gate_ws, Config} | List], Config);
web_path_list([{static, Path, Dir} | Tail], List, Config)
    when is_list(Path), is_list(Dir) ->
    BinDir = list_to_binary(Dir),
    BinPath = list_to_binary(Path),
    PatternList = path_to_pattern_list(BinPath, BinDir),
    web_path_list(Tail, PatternList ++ List, Config);
web_path_list([{static, _Path, _Dir} | Tail], List, Config) ->
    web_path_list(Tail, List, Config).

path_to_pattern_list(Path, Dir) ->
    Result = file:list_dir(Dir),
    path_to_pattern_list(Path, Result, Dir, []).

path_to_pattern_list(_Path, [], _Dir, PatternList) ->
    PatternList;
path_to_pattern_list(Path, [File | Tail], Dir, PatternList) ->
    FileDir = filename:join(Dir, File),
    {ok, #file_info{type = Type}} = file:read_file_info(FileDir),
    NewPatternList = add_file_to_pattern_list(File, Path, Type, FileDir, PatternList),
    path_to_pattern_list(Path, Tail, Dir, NewPatternList);
path_to_pattern_list(Path, {ok, FileList}, Dir, PatternList) ->
    path_to_pattern_list(Path, FileList, Dir, PatternList);
path_to_pattern_list(_Path, {error, _Reason}, _Dir, _PatternList) ->
    [].

add_file_to_pattern_list(File, Path, regular, FileDir, PatternList) ->
    FilePath = filename:join(Path, File),
    NewPatternList = [{FilePath, cowboy_static, {file, FileDir}} | PatternList],
    maybe_add_path(File, Path, FileDir, NewPatternList);
add_file_to_pattern_list(Dir, Path, directory, FileDir, PatternList) ->
    DirPath = filename:join(Path, Dir),
    Pattern = dir_to_pattern(DirPath, binary:last(DirPath)),
    [{Pattern, cowboy_static, {dir, FileDir}} | PatternList];
add_file_to_pattern_list(_File, _Path, _Type, _Dir, PatternList) ->
    PatternList.

maybe_add_path(File, Path, FileDir, List) when is_list(File) ->
    maybe_add_path(list_to_binary(File), Path, FileDir, List);
maybe_add_path(<<"index.html">>, Path, FileDir, List) ->
    [{Path, cowboy_static, {file, FileDir}} | List];
maybe_add_path(_, _, _, List) ->
    List.

dir_to_pattern(BinDir, $/) ->
    Pattern = <<"[...]">>,
    <<BinDir/binary, Pattern/binary>>;
dir_to_pattern(BinDir, _) ->
    Slash = <<"/">>,
    WithSlash = <<BinDir/binary, Slash/binary>>,
    dir_to_pattern(WithSlash, $/).

web_options(UseSSL, Config) ->
    Port = maps:get(port, Config, 8080),
    Inet6 = maps:get(inet6, Config, true),

    Cert = cert_file(Config),
    Key = key_file(Config),

    NumAcceptors = maps:get(num_acceptors, Config, 5),
    ctg_con_utils:options([{inet6, Inet6},
                           {port, Port},
                           {certfile, Cert},
                           {keyfile, Key},
                           {num_acceptors, NumAcceptors}],
                          [],
                          UseSSL).

start_tls_or_clear(true, Name, Options, Dispatch) ->
    {ok, _} = cowboy:start_tls(Name, Options, #{env => #{dispatch => Dispatch}}),
    ok;
start_tls_or_clear(false, Name, Options, Dispatch) ->
    {ok, _} = cowboy:start_clear(Name, Options, #{env => #{dispatch => Dispatch}}),
    ok.

cert_file(Config) ->
    maps:get(cert_file, Config, undefined).

key_file(Config) ->
    maps:get(key_file, Config, undefined).
