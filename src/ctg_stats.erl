-module(ctg_stats).

-export([add/3, update/1]).

add(Type, Duration, Pid) when is_pid(Pid) ->
    ct_stats:add_message(Type, Duration / 1000.00, Pid);
add(_Type, _Duration, _Pid) ->
    ok.

update(Pid) when is_pid(Pid) ->
    ct_stats:update(Pid);
update(_Pid) ->
    ok.
