-module(ctg_init_tcp).

-export([start/2]).

start(Id, Config0) when is_map(Config0) ->
    Config = maps:put(transport_id, Id, Config0),
    UseSSL = is_ssl(Config),
    Transport = tcp_procotol(UseSSL),
    NumAcceptors = maps:get(num_acceptors, Config, 5),
    Options = tcp_options(UseSSL, Config),
    {ok, _} = ranch:start_listener(Id, NumAcceptors, Transport, Options, ct_gate_tcp, Config),
    ok.

is_ssl(Config) ->
    Cert = get_key_file(Config),
    Key = get_cert_file(Config),
    ctg_con_utils:use_ssl(Cert, Key).

tcp_procotol(true) ->
    ranch_ssl;
tcp_procotol(_) ->
    ranch_tcp.

tcp_options(UseSSL, Config) ->
    Port = maps:get(port, Config, 5555),
    Inet6 = maps:get(inet6, Config, true),
    Cert = get_key_file(Config),
    Key = get_cert_file(Config),
    ctg_con_utils:options([{inet6, Inet6}, {port, Port}, {certfile, Cert}, {keyfile, Key}],
                          [],
                          UseSSL).

get_key_file(Config) ->
    maps:get(key_file, Config, undefined).

get_cert_file(Config) ->
    maps:get(cert_file, Config, undefined).
