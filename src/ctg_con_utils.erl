-module(ctg_con_utils).

-export([options/3, use_ssl/2]).

options([], Options, _SSL) ->
    clean_options(Options);
options([{port, _} = Port | T], Options, SSL) ->
    options(T, [Port | Options], SSL);
options([{inet6, true} | T], Options, SSL) ->
    options(T, [inet6 | Options], SSL);
options([{num_acceptors, _} = Opt | T], Options, SSL) ->
    options(T, [Opt | Options], SSL);
%% SSL options
options([{certfile, undefined} | T], Options, true) ->
    options(T, Options, true);
options([{certfile, _} = Cert | T], Options, true) ->
    options(T, [Cert | Options], true);
options([{keyfile, undefined} | T], Options, true) ->
    options(T, Options, true);
options([{keyfile, _} = Key | T], Options, true) ->
    options(T, [Key | Options], true);
options([_ | T], Options, SSL) ->
    options(T, Options, SSL).

clean_options(Options) ->
    remove_bad_ipv6(Options).

remove_bad_ipv6(Options) ->
    Entries = [{ipv6_v6only, false}, {ipv6_v6only, true}],
    Delete =
        fun (Entry, Opts) ->
                lists:delete(Entry, Opts)
        end,
    lists:foldl(Delete, Options, Entries).

use_ssl(Cert, Key) ->
    filelib:is_regular(Cert) and filelib:is_regular(Key).
