-module(ct_gate).

-export([start_transports/1]).

start_transports(Config) ->
    Ids = maps:keys(Config),
    start_transports(Ids, Config).

start_transports([], _Config) ->
    ok;
start_transports([Id | T], Config) ->
    lager:info("starting transport ~p", [Id]),
    TransConf = maps:get(Id, Config),
    TransportConfig = ensure_mandatory_settings(TransConf),
    start_transport(Id, TransportConfig),
    start_transports(T, Config).

start_transport(Id, #{type := <<"rawtcp">>} = Config) ->
    ctg_init_tcp:start(Id, Config);
start_transport(Id, #{type := <<"websocket">>} = Config) ->
    ctg_init_web:start(Id, Config);
start_transport(Id, #{type := Type}) ->
    lager:error("transport ~p has unknown transport type ~p", [Id, Type]),
    throw({unknown_transport_type, Id, Type}).

ensure_mandatory_settings(Config) ->
    MandatoryKeys = [port],
    Default = #{enable_stats => false, router_if => ct_router_if_local},
    KeysMissing =
        fun (Key, Missing) ->
                case maps:is_key(Key, Config) of
                  true ->
                      Missing;
                  false ->
                      [Key | Missing]
                end
        end,
    [] = lists:foldl(KeysMissing, [], MandatoryKeys),

    maps:merge(Default, Config).
