%%
%% Copyright (c) 2017-2019 Bas Wegh

-module(ct_gate_tcp).

-include_lib("ct_msg/include/ct_msg.hrl").

-behaviour(gen_server).
-behaviour(ranch_protocol).

%% for ranch_protocol
-export([start_link/4]).
%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([code_change/3]).
-export([terminate/2]).

-record(state,
        {gate_in = undefined,
         socket = undefined,
         transport = undefined,
         serializer = undefined,
         remote_max_length = undefined,
         handshake_performed = false,
         max_length_exp = undefined}).

start_link(Ref, Socket, Transport, Conf) ->
    {ok, proc_lib:spawn_link(?MODULE, init, [{Ref, Socket, Transport, Conf}])}.

init({Ref, Socket, Transport, Config}) ->
    ok = ranch:accept_ack(Ref),
    {ok, {IP, Port}} = peername(Transport, Socket),
    PeerCert = peercert(Transport, Socket),
    MaxLengthExp = maps:get(max_length_exp, Config, 15),
    Update =
        #{peer_ip => IP, peer_port => Port, peer_cert => PeerCert, transport_type => rawsocket},
    InConfig = maps:merge(Config, Update),
    {ok, Pid} = ct_gate_in:start_link(InConfig),
    State =
        #state{gate_in = Pid,
               socket = Socket,
               transport = Transport,
               % (x+9) ** 2 is the lengh
               % so
               %   0 -> 2 ** 9
               %   1 -> 2 ** 10 = 1024 etc.
               % the number gets shifted 4 bits to the left.
               % 15 is the max receive length possible ~ 16M (2 ** 24).
               max_length_exp = MaxLengthExp},
    connection_active_once(State),
    gen_server:enter_loop(?MODULE, [], State).

handle_info({tcp, Socket, <<127, MaxLengthExp:4, SerializerNumber:4, 0, 0>>},
            State = #state{socket = Socket, handshake_performed = false}) ->
    % handshake
    handle_handshake_message(MaxLengthExp, SerializerNumber, State);
handle_info({tcp, Socket, _Data},
            State = #state{socket = Socket, handshake_performed = false}) ->
    lager:debug("[~p] stopping due to bad handshake", [self()]),
    {stop, normal, State};
handle_info({tcp, Socket, Data}, State = #state{socket = Socket, gate_in = Pid})
    when byte_size(Data) >= 1 ->
    ct_gate_in:handle_raw_data(Data, Pid),
    {noreply, State};
handle_info(connection_once, State) ->
    connection_active_once(State),
    {noreply, State};
handle_info({connection_send, Data}, State) ->
    connection_send(Data, State),
    {noreply, State};
handle_info({tcp_closed, Socket}, State = #state{socket = Socket}) ->
    lager:info("[~p] stopping due to tcp closed", [self()]),
    {stop, normal, State};
handle_info({tcp_error, Socket, Error}, State = #state{socket = Socket}) ->
    lager:warn("[~p] stopping due to tcp error ~p", [self(), Error]),
    {stop, normal, State};
handle_info(Info, State) ->
    lager:warn("[~p] stopping due to unknown info ~p", [self(), Info]),
    {stop, normal, State}.

handle_call(_, _, State) ->
    {reply, ignored, State}.

handle_cast(_, State) ->
    {noreply, State}.

terminate(_Reason, State) ->
    connection_close(State),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

peername(ranch_ssl, Socket) ->
    ranch_ssl:peername(Socket);
peername(ranch_tcp, Socket) ->
    ranch_tcp:peername(Socket).

peercert(ranch_ssl, Socket) ->
    Result = ssl:peercert(Socket),
    ensure_cert(Result);
peercert(ranch_tcp, _Socket) ->
    none.

ensure_cert({ok, Cert}) ->
    Cert;
ensure_cert(Error) ->
    lager:debug("~p: error getting peer cert: ~p", [self(), Error]),
    none.

connection_active_once(#state{transport = ranch_ssl, socket = Socket}) ->
    ranch_ssl:setopts(Socket, [{active, once}]);
connection_active_once(#state{transport = ranch_tcp, socket = Socket}) ->
    ranch_tcp:setopts(Socket, [{active, once}]).

connection_send(Data, #state{transport = ranch_ssl, socket = Socket}) ->
    ranch_ssl:send(Socket, Data);
connection_send(Data, #state{transport = ranch_tcp, socket = Socket}) ->
    ranch_tcp:send(Socket, Data).

connection_close(#state{transport = ranch_ssl, socket = Socket, gate_in = Pid}) ->
    ct_gate_in:stop(Pid),
    ranch_ssl:close(Socket);
connection_close(#state{transport = ranch_tcp, socket = Socket, gate_in = Pid}) ->
    ct_gate_in:stop(Pid),
    ranch_tcp:close(Socket).

handle_handshake_message(MaxLengthExp, SerializerNumber, State) ->
    lager:debug("[~p] handle handshake", [self()]),
    Serializer = translate_serializer_number_to_name(SerializerNumber),
    MaxLength = calculate_max_length(MaxLengthExp),
    send_handshake_reply(Serializer, State),
    NewState = State#state{serializer = Serializer, remote_max_length = MaxLength},
    activate_or_close_connection(Serializer, NewState).

activate_or_close_connection(unsupported, State) ->
    {stop, normal, State};
activate_or_close_connection(Serializer, #state{gate_in = Pid} = State) ->
    connection_active_once(State),
    ok = ct_gate_in:set_serializer(Serializer, Pid),
    {noreply, State#state{handshake_performed = true}}.

translate_serializer_number_to_name(1) ->
    raw_json;
translate_serializer_number_to_name(2) ->
    raw_msgpack;
translate_serializer_number_to_name(_) ->
    unsupported.

calculate_max_length(MaxLengthExp) ->
    round(math:pow(2, 9 + MaxLengthExp)).

send_handshake_reply(raw_json, #state{max_length_exp = Length} = State) ->
    connection_send(use_raw_json_message(Length), State);
send_handshake_reply(raw_msgpack, #state{max_length_exp = Length} = State) ->
    connection_send(use_raw_msgpack_message(Length), State);
send_handshake_reply(_, State) ->
    connection_send(unsupported_serializer_message(), State).

unsupported_serializer_message() ->
    <<127, 1:4, 0:4, 0, 0>>.

%% max_length_unacceptable_message() ->
%%     <<127,2:4,0:4,0,0>>.

%% use_reserved_bits_message() ->
%%     <<127,3:4,0:4,0,0>>.

%% max_connections_message() ->
%%     <<127,4:4,0:4,0,0>>.

use_raw_msgpack_message(LengthExp) ->
    <<127, LengthExp:4, 2:4, 0, 0>>.

use_raw_json_message(LengthExp) ->
    <<127, LengthExp:4, 1:4, 0, 0>>.
